import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Submission } from '../services/classes/submission';
import { freeApiService } from '../services/freeapi.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  constructor(private _freeApiService: freeApiService, private router: Router) {
  }

  lstSubmissions:Submission[];
  title = 'Hackernews';
  ngOnInit() {
    this._freeApiService.getNew()
    .subscribe
 (
      data =>
      {
        this.lstSubmissions = data;
      }
    );
  }

  onSelect(id) {
    this.router.navigate(['/profile', id]);
}

GoThreads(id) {
  this.router.navigate(['/threads', id]);
}

}

import { Component, OnInit } from '@angular/core';
import { freeApiService } from '../services/freeapi.service';
import { Commentary } from '../services/classes/commentary';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';

@Component({
    selector: 'app-threads',
    templateUrl: './threads.component.html',
    styleUrls: ['./threads.component.css']
})
export class ThreadsComponent implements OnInit {

    constructor(private _freeApiService: freeApiService, private router: Router) {
    }
  
    lstComments:Commentary[];
    title = 'Hackernews';
    ngOnInit() {
      this._freeApiService.getThreads(6)
      .subscribe
      (
        data =>
        {
          this.lstComments = data;
        }
      );
    }

    GoThreads(id) {
      this.router.navigate(['/threads', id]);
    }

    onSelect(id) {
      this.router.navigate(['/profile', id]);
  }
  }
export class Profile {
    id: number;
    karma: number;
    created: string;
    about: string;
    user: {
        id: number;
        username: string;
        email: string;
    }
}
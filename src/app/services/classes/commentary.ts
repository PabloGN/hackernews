export class Commentary {
    text: string;
    created: string;
    contribution: {
        id: number;
        title: string;
    };
    user: {
        id: number;
        user: {
            id: number;
            username: string;
            email: string;
        };
    };
    // tslint:disable-next-line: variable-name
    father_comm: number;
    replies: Commentary[];
    likes: [
        id: number,
    ];
    id: number;
}
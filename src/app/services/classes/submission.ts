export class Submission {
    id: number;
    title: string;
    date: Date;
    user: {
        id: number;
        user: {
            id: number;
            username: string;
            email: string;
        };
    };
    likes: [
        id: number,
    ];
    ncomments: number;
    url: string;
    ask: string;
}
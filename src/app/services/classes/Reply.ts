export class Reply {
    text: string;
    created: Date;
    contribution: number;
    user: number;
    // tslint:disable-next-line: variable-name
    father_comm: number;
    likes: [
        id: number,
    ];
    replies: [
        reply: Reply,
    ];
    id: number;
}
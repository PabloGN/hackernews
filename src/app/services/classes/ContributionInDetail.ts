import { Submission } from './../../services/classes/submission';
import { Commentary } from './commentary';
import { Reply } from './Reply';

export class ContributionInDetail {
    id: number;
    title: string;
    url: string;
    ask: string;
    date: Date;
    user: {
        id: number;
        user: {
            id: number;
            username: string;
            email: string;
        };
    };
    likes: [
        id: number,
    ];
    ncomments: number;
    commentas: [{
        text: string,
        created: Date,
        contribution: number;
        user: number;
        father_comm: number;
        likes: [
            id: number,
        ];
        replies: [
               reply: Reply,
        ];
        id: number;
    }
    ];
}

import { Commentary } from './classes/commentary';
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Profile } from './classes/profile';
import { ContributionInDetail } from './classes/ContributionInDetail';

@Injectable()
export class freeApiService {
    /*
    getProfile() {
        throw new Error('Method not implemented.');
    }*/

    constructor(private httpclient: HttpClient){ }


      // tslint:disable-next-line: adjacent-overload-signatures
    getProfile(profileId): Observable<any> {
        const endpointURL = 'https://sheltered-badlands-07979.herokuapp.com/profile/' + profileId;
        return this.httpclient.get(endpointURL);
    }
    getContributionDetail(idContribution): Observable<any> {
        const endpointURL = 'https://sheltered-badlands-07979.herokuapp.com/ContributionDetail/' + idContribution;
        return this.httpclient.get(endpointURL);
    }
    updateProfile(profileId, body): Observable<any> {
        const headers = {'Authorization': '106271286983983058622'};
        console.log("Sending put request")
        const endpointURL = 'https://sheltered-badlands-07979.herokuapp.com/profileupdate/' + profileId + '/';
        return this.httpclient.put(endpointURL, body, {headers});
    }

    getSubmissions(profileId): Observable<any> {
        const endpointURL = "https://sheltered-badlands-07979.herokuapp.com/Submissions/" + profileId;
        return this.httpclient.get(endpointURL);
    }
    upvotedComments(profileId): Observable<any>{
        const endpointURL = "https://sheltered-badlands-07979.herokuapp.com/upvotedCommentaries/" + profileId;
        return this.httpclient.get(endpointURL);
    }

    upvotedSubmissions(profileId): Observable<any>{
        const endpointURL = "https://sheltered-badlands-07979.herokuapp.com/upvotedSubmissions/" + profileId;
        return this.httpclient.get(endpointURL);
    }

    getHome(): Observable<any> {
        return this.httpclient.get('https://sheltered-badlands-07979.herokuapp.com/home/');
    }

    getNew(): Observable<any> {
        return this.httpclient.get("https://sheltered-badlands-07979.herokuapp.com/new/");
    }

    getAsk(): Observable<any> {
        return this.httpclient.get('https://sheltered-badlands-07979.herokuapp.com/ask/');
    }

    likeContribution(idContribution, c: ContributionInDetail): Observable<any> {
        const headers = {'Authorization': '106271286983983058622'};
        console.log("Sending post request")
        const endpointURL = 'https://sheltered-badlands-07979.herokuapp.com/LikeContribution/' + idContribution;
        return this.httpclient.put(endpointURL, c);
    }

    UnlikeContribution(idContribution, c: ContributionInDetail): Observable<any> {
        const headers = {'Authorization': '106271286983983058622'};
        console.log("Sending post request")
        const endpointURL = 'https://sheltered-badlands-07979.herokuapp.com/UnlikeContribution/' + idContribution;
        return this.httpclient.put(endpointURL, c);
    }

    likeCommentary(idCommentary, c: Commentary): Observable<any> {
        const headers = {'Authorization': '106271286983983058622'};
        console.log("Sending post request")
        const endpointURL = 'https://sheltered-badlands-07979.herokuapp.com/LikeCommentary/' + idCommentary;
        return this.httpclient.put(endpointURL, c);
    }

    UnlikeCommentary(idCommentary, c: Commentary): Observable<any> {
        const headers = {'Authorization': '106271286983983058622'};
        console.log("Sending post request")
        const endpointURL = 'https://sheltered-badlands-07979.herokuapp.com/UnlikeCommentary/' + idCommentary;
        return this.httpclient.put(endpointURL, c);
    }

    getThreads(profileId): Observable<any> {
        return this.httpclient.get(
            "https://sheltered-badlands-07979.herokuapp.com/threads/" + profileId
        )
    }

    addCommentary(body){
        const headers = {'Authorization': '106271286983983058622'};
        console.log("Sending post request")
        return this.httpclient.post(
            'https://sheltered-badlands-07979.herokuapp.com/AddComment/',
            body,
            {headers}
        );
    }
    postSubmision(body){
        const headers = {'Authorization': '106271286983983058622'};
        console.log("Sending post request")
        return this.httpclient.post(
            "https://sheltered-badlands-07979.herokuapp.com/submit/",
            body,
            {headers}
        );
    }
    
    /*
    getContributionDetail(idContibution): Observable<any> {
        return this.httpclient.get('https://sheltered-badlands-07979.herokuapp.com/ContributionDetail/1');
    }*/
}
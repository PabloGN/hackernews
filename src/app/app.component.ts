import { Component } from '@angular/core';
import { Submission } from './services/classes/submission';
import { freeApiService } from './services/freeapi.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private _freeApiService: freeApiService) {
  }

  lstSubmissions: Submission[];
  title = 'Hackernews';
  ngOnInit() {
    this._freeApiService.getHome()
    .subscribe
 (
      data =>
      {
        this.lstSubmissions = data;
      }
    );
  }
}

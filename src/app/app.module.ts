import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { freeApiService } from './services/freeapi.service';
import { FormsModule } from '@angular/forms';
import { RouterModule , Route} from '@angular/router';
import { ContributionDetailComponent } from './contribution-detail/contribution-detail.component';
import { HomeComponent } from './home/home.component';
import { NewComponent } from './new/new.component';
import { AskComponent } from './ask/ask.component';
import { ThreadsComponent } from './threads/threads.component';
import { SubmitComponent } from './Submit/submit.component';
import { ProfileComponent } from './Profile/profile.component';
import { SubmissionsComponent } from './Submissions/submissions.component';
import { UpvotedSubmissionsComponent } from './upvotedSubmissions/upvotedSubmissions.component';
import { UpvotedCommentsComponent } from './upvotedComments/upvotedComments.component';


const routes: Route[] = [
  {path: '', component: HomeComponent},
  {path: 'contribution/:id', component: ContributionDetailComponent},
  {path: 'new', component: NewComponent},
  {path: 'ask', component: AskComponent},
  {path: 'profile/:id', component: ProfileComponent},
  {path: 'submissions/:id', component: SubmissionsComponent},
  {path: 'upvotedSubmissions/:id', component: UpvotedSubmissionsComponent},
  {path: 'upvotedComments/:id', component: UpvotedCommentsComponent},
  {path: 'submit', component: SubmitComponent},
  {path: 'threads/:id', component: ThreadsComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    ContributionDetailComponent,
    HomeComponent,
    NewComponent,
    AskComponent,
    ProfileComponent,
    SubmissionsComponent,
    UpvotedCommentsComponent,
    UpvotedSubmissionsComponent,
    ThreadsComponent,
    SubmitComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  exports:[RouterModule],
  providers: [freeApiService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }

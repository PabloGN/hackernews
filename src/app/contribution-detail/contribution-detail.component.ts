import { comentForm } from './../classes/comentForm';
import { Commentary } from './../services/classes/commentary';
import { ContributionInDetail } from './../services/classes/ContributionInDetail';
import { Submission } from './../services/classes/submission';
import { Component, OnInit } from '@angular/core';
import { freeApiService } from '../services/freeapi.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-contribution-detail',
  templateUrl: './contribution-detail.component.html',
  styleUrls: ['./contribution-detail.component.css']
})
export class ContributionDetailComponent implements OnInit {
  Commentary: any;
  errorMessage: any;

  // tslint:disable-next-line: variable-name
  constructor(private _freeApiService: freeApiService, private rutaActiva: ActivatedRoute, private router: Router) {
  }
  Contribution: ContributionInDetail;
  lstSubmissions: Submission;
  model: comentForm = {text: ''};
  // tslint:disable-next-line: typedef
  ngOnInit() {
    //alert(this.rutaActiva.snapshot.paramMap.get('id'));
    // this._freeApiService.getHome()

    // tslint:disable-next-line: radix
    let id = parseInt(this.rutaActiva.snapshot.paramMap.get('id'));
    this._freeApiService.getContributionDetail(id).subscribe(data =>{
        this.Contribution = data;
    });


    /*
    this._freeApiService.getContributionDetail(this.rutaActiva.snapshot.params.id)
    .subscribe(data => {
      this.Contribution = data;
  });*/
  }

  // tslint:disable-next-line: typedef
  likeContribution(id: number){
    const c = this.Contribution;
    this._freeApiService.likeContribution(id, c).subscribe(data => {
      this.Contribution = data;
  });
  }

  // tslint:disable-next-line: typedef
  UnlikeContribution(id: number){
    const c = this.Contribution;
    this._freeApiService.UnlikeContribution(id, c).subscribe(data => {
      this.Contribution = data;
  });
  }

  likeCommentary(id: number){
    // cambiar
    const c = this.Commentary;
    this._freeApiService.likeCommentary(id, c).subscribe(data => {
      this.Contribution = data;
  });
  }

  // tslint:disable-next-line: typedef
  UnlikeCommentary(id: number){
   // cambiar
    const c = this.Commentary;
    this._freeApiService.UnlikeCommentary(id, c).subscribe(data => {
      this.Contribution = data;
  });
  }

  addCommentary() {
    console.log(this.model);
        // tslint:disable-next-line: align
        this._freeApiService.postSubmision(this.model).subscribe({
            error: error => {
                this.errorMessage = error.message;
                console.error('There was an error', error);
                if (error.status == 401) {
                  console.log("Unauthorized");
              }
             else if (error.status == 402) {
                console.log("Contribution inexistente");
            }
             else  {
                    console.log("Redirecting to new");
                    this.router.navigate(['/contribution/'+ this.Contribution.id]);
                }
              
            }
        })
        
  }
}

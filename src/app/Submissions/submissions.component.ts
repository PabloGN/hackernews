import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { freeApiService } from '../services/freeapi.service';
import {Submission} from '../services/classes/submission';
import { ActivatedRoute, Router } from '@angular/router';
import { Profile } from '../services/classes/profile';

@Component({
  selector: 'submissions',
  templateUrl: './submissions.component.html',
  styleUrls: ['./submissions.component.css']
})

export class SubmissionsComponent implements OnInit{
    listsubmissions: Submission[];
    profile: Profile;

    constructor(private _freeApiService: freeApiService, private route: ActivatedRoute, private router: Router) {}

    ngOnInit() {
        let id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._freeApiService.getSubmissions(id).subscribe(data=>{
            this.listsubmissions = data;
        });
        this._freeApiService.getProfile(id).subscribe(data=>{
            this.profile = data;
        });
        
    }

    onSelect(id) {
        this.router.navigate(['/profile', id]);
    }
    
    GoSubmissions(id) {
        this.router.navigate(['/submissions', id]);
    }

    GoThreads(id) {
        this.router.navigate(['/threads', id]);
    }

}
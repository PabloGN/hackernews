import { Component, OnInit } from '@angular/core';
import { freeApiService } from '../services/freeapi.service';
import { Submission } from '../services/classes/submission';
import { Router } from '@angular/router';

@Component({
    selector: 'app-ask',
    templateUrl: './ask.component.html',
    styleUrls: ['./ask.component.css']
})
export class AskComponent implements OnInit {

    constructor(private _freeApiService: freeApiService, private router: Router) {
    }
  
    lstSubmissions:Submission[];
    title = 'Hackernews';
    ngOnInit() {
      this._freeApiService.getAsk()
      .subscribe
   (
        data =>
        {
          this.lstSubmissions = data;
        }
      );
    }

    onSelect(id) {
      this.router.navigate(['/profile', id]);
  }

  GoThreads(id) {
    this.router.navigate(['/threads', id]);
  }
}
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { freeApiService } from '../services/freeapi.service';
import {Commentary} from '../services/classes/commentary';
import { ActivatedRoute, Router } from '@angular/router';
import { Profile } from '../services/classes/profile';

@Component({
  selector: 'upvoted-comments',
  templateUrl: './upvotedComments.component.html',
  styleUrls: ['./upvotedComments.component.css']
})
export class UpvotedCommentsComponent implements OnInit{

    upvotedComments: Commentary[];
    profile: Profile;

    constructor(private _freeApiService: freeApiService, private route: ActivatedRoute, private router: Router) {}

    ngOnInit() {
        let id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._freeApiService.upvotedComments(id).subscribe(data => {
            this.upvotedComments = data;
        });
        this._freeApiService.getProfile(id).subscribe(data=>{
            this.profile = data;
        });
    }

    onSelect(id) {
        this.router.navigate(['/profile', id]);
    }

    GoThreads(id) {
        this.router.navigate(['/threads', id]);
      }
}
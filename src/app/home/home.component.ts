import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Submission } from '../services/classes/submission';
import { freeApiService } from '../services/freeapi.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _freeApiService: freeApiService, private router: Router) {
  }

  lstSubmissions:Submission[];
  title = 'Hackernews';
  ngOnInit() {
    this._freeApiService.getHome()
    .subscribe
 (
      data =>
      {
        this.lstSubmissions = data;
      }
    );
  }

  onSelect(id) {
      this.router.navigate(['/profile', id]);
  }

  GoThreads(id) {
    this.router.navigate(['/threads', id]);
  }
}

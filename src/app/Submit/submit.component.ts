import { Component, OnInit, ɵɵqueryRefresh } from '@angular/core';
import { freeApiService } from '../services/freeapi.service';
import { CommonModule } from '@angular/common';
import { Observable, throwError } from 'rxjs';
import { submitForm } from '../classes/submitForm';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'app-submit',
    templateUrl: './submit.component.html',
    styleUrls: ['./submit.component.css']
})
export class SubmitComponent implements OnInit {
    errorMessage: any;

    constructor(private _freeApiService: freeApiService, private router: Router) {
    }
  
    title = 'Hackernews';
    lstMessages = [];
    ngOnInit() {
        this.lstMessages = [];
    }

    model: submitForm = {title: '', url: '', text: ''};

    onSubmit(){
        console.log(this.model);
        this._freeApiService.postSubmision(this.model).subscribe({
            error: error => {
                this.errorMessage = error.message;
                console.error('There was an error', error);
                if (error.status == 400) {
                    if(error.error == "Field title is missing") {
                        console.log("Falta titulo");
                        this.lstMessages[0] = "Title is missing";
                        this.router.navigate(['/submit']);
                    }
                    else if (error.error == "The url is not valid") {
                        console.log("The url is not valid");
                        this.lstMessages[0] = "Url not valid";
                        this.router.navigate(['/submit']);
                    }
                }
                else if (error.status == 302) {
                    console.log("Redirecting to contribution detail " + error.error.contribution.id);
                    this.router.navigate(['/contribution', error.error.contribution.id]);
                }
                else if (error.status == 401) {
                    console.log("Unauthorized");
                    this.lstMessages[0] = "Unauthorized";
                        this.router.navigate(['/submit']);
                }
                else {
                    this.lstMessages[0] = "Unkown error";
                    this.router.navigate(['/submit']);
                    console.log("Unkown error");
                }
            }, 
            next: next =>  {
                console.log("Redirecting to new");
                this.router.navigate(['/new']);
            }
        })
        
    }
    GoThreads(id) {
        this.router.navigate(['/threads', id]);
      }

      onSelect(id) {
        this.router.navigate(['/profile', id]);
    }

    
  }
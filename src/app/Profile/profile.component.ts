import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import {Profile} from '../services/classes/profile';
import { ActivatedRoute, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { stringify } from 'querystring';
import { ProfileUpdate } from '../services/classes/profileUpdate';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    checkoutForm;
    profile: Profile;
    model: ProfileUpdate = {about: ""};

    constructor(private _freeApiService: freeApiService, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        let id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._freeApiService.getProfile(id).subscribe(data=>{
            this.profile = data;
        });
        
    }

    updateProfile(profileId) {
        console.log(this.model);
        this._freeApiService.updateProfile(profileId, this.model).subscribe(data => {
            this.profile = data;
        });

        //this.ngOnInit();
    }

    onSelect(id) {
        this.router.navigate(['/profile', id]);
    }

    GoSubmissions(id) {
        this.router.navigate(['/submissions', id]);
    }

    GoUpvotedS(id) {
        this.router.navigate(['/upvotedSubmissions', id]);
    }

    GoUpvotedC(id) {
        this.router.navigate(['/upvotedComments', id]);
    }

    GoThreads(id) {
        this.router.navigate(['/threads', id]);
      }
}
